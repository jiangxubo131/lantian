import styled from 'styled-components'
const Title = styled.div`
  .top{
    background-color: black;
    width: calc(100% - 200px);
    height: 80px;
    position: absolute;
    right: 0;
    /* top: 0; */
  }
  .left{
      position: absolute;
      left: 0;
      height:100%;
      width: 200px;
      /* background-color: blue; */
      z-index: 99;
  }
  .bo{
    margin-top: 80px;
    position: absolute;
    right: 0;
    height: calc(100% - 80px);
    width:  calc(100% - 200px);
    min-width:800px ;
    min-height:500px ;
    background-color: #eee;
  }
`
export {
  Title
}
