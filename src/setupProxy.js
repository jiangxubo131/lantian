const { createProxyMiddleware: proxy } = require('http-proxy-middleware')
module.exports = app => {
  // express环境
  app.use('/api', proxy({
    target: 'http://localhost:9090',
    changeOrigin: true,
    pathRewrite: {}
  }))
}


