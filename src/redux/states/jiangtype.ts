export enum ActionCreator {
    INCR = 'incr',
    ADD = 'add',
    DEL = 'del',
    DONE = 'done'
  }