import {redux} from "../states/jiang"
import jiang from "../methods/jiang"
  
  export default (state = redux, action:any):any => {
   try{
     return jiang[action.type](state,action.data)
   }
    catch(err){
      return state
    }
  }