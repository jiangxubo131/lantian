// all 在主程序saga中执行多个监听saga
import { all } from 'redux-saga/effects'

// 电影的saga
import filmSaga from '@/views/Film/saga'

// 入口程序 主saga 它是一个函数，generator函数
function* mainSaga() {
  // 写监听的saga
  yield all([
    filmSaga()
  ])
}

export default mainSaga