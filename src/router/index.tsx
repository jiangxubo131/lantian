import React, { FC, ReactElement } from 'react'
import { Route, Switch, Redirect, Link } from 'react-router-dom'

import Zhuye from '../views/zhuye'
import Ri from '../views/rizhiguanli'

import Gongneng from '../views/xitongpeizhi/gongneng'
import Renyuan from '../views/xitongpeizhi/renyuan'
import Shuju from '../views/xitongpeizhi/shuju'
import Xitong from '../views/xitongpeizhi/xitong'
import Caozuo from '../views/rizhiguanli/caozuo'
import Denglu from '../views/rizhiguanli/denglu'
import Yicang from '../views/rizhiguanli/yichang'

import Yemian from '../views/houtai/yemian'
import Caidan from '../views/houtai/caidan'

const Rout: FC = (): ReactElement => {
    return (
        <div>
            <Switch>
                <Route path="/shouye" component={Zhuye} />
                <Route path="/Gongneng" component={Gongneng} />
                <Route path="/Renyuan" component={Renyuan} />
                <Route path="/Shuju" component={Shuju} />
                <Route path="/Xitong" component={Xitong} />
                <Route path="/Caozuo" component={Caozuo} />
                <Route path="/Denglu" component={Denglu} />
                <Route path="/Yicang" component={Yicang} />
                <Route path="/Yemian" component={Yemian} />
                <Route path="/Caidan" component={Caidan} />

                <Route path="/rizhi" component={Ri} />
                < Redirect to="/shouye" > </Redirect>

            </Switch>

        </div>
    );
}

export default Rout;
