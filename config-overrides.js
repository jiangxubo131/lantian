const path = require('path')
// const {
//   addDecoratorsLegacy,
//   override,
//   addWebpackAlias,
//   fixBabelImports
// } = require('customize-cra')

module.exports = override(
  addDecoratorsLegacy(),
  addWebpackAlias({
    '@': path.join(__dirname, 'src')
  }),
  // 按需加载antd组件库所用
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: 'css',
  })
)


